.PHONY: help
.DEFAULT_GOAL = help

## —— Global ⭕ ——————————————————————————————————————————————————————————————————
add_credentials: ## Add credentials in ~/.aws
	docker run --rm -it -v $(shell pwd):/aws -v ~/.aws:/root/.aws amazon/aws-cli configure import --csv file://credentials.csv

initialize: backend_init network_init instance_init script_init start ## Initialize all layers
start: backend_start network_start instance_start delete_agent ansible_install ## Mount network, servers and install playbook
destroy: script_down instance_down network_down  ## Destroy all

## —— Backend 💾 ——————————————————————————————————————————————————————————————————
backend_init: ## Initialization of backend
	terraform -chdir=terraform/00-backend init

backend_start: ## Apply s3 and dynamo
	terraform -chdir=terraform/00-backend apply --auto-approve

## —— Network 📡 ——————————————————————————————————————————————————————————————————
network_init: ## Initialization of network
	terraform -chdir=terraform/10-network init

network_start: ## Apply networking
	terraform -chdir=terraform/10-network apply --auto-approve

network_down: ## Destroy networking
	terraform -chdir=terraform/10-network destroy --auto-approve

network_lint: ## Linter network
	terraform -chdir=terraform/10-network fmt

## —— Compute 🖥 ——————————————————————————————————————————————————————————————————
instance_init: ## Initialization of instances
	terraform -chdir=terraform/20-instances init

instance_dry_run: ## dry run instances
	terraform -chdir=terraform/20-instances plan

instance_start: ## Apply instances
	terraform -chdir=terraform/20-instances apply --auto-approve

instance_restart: 
	terraform -chdir=terraform/20-instances apply --auto-approve \
	&& terraform -chdir=terraform/30-scripts apply --auto-approve

instance_down: script_down ## Destroy instances
	terraform -chdir=terraform/20-instances destroy --auto-approve

instance_lint: ## Linter instance
	terraform -chdir=terraform/20-instances fmt

## —— Script 📝 ——————————————————————————————————————————————————————————————————
script_init: ## Initialization of instances
	terraform -chdir=terraform/30-scripts init

script_dry_run: ## dry run scripts
	terraform -chdir=terraform/30-scripts plan

script_down: ## Destroy scripts
	terraform -chdir=terraform/30-scripts destroy --auto-approve

script_lint: ## Linter scripts
	terraform -chdir=terraform/30-scripts fmt

script_start:
	terraform -chdir=terraform/30-scripts apply --auto-approve

## —— Ansible 📥 ——————————————————————————————————————————————————————————————————
ansible_install: ## Execute playbook in all servers
	terraform -chdir=terraform/30-scripts apply --auto-approve \
	&& cd ansible_config \
	&& ansible-playbook playbook.yml

## —— Connection ssh 📥 ———————————————————————————————————————————————————————————
connect_s0: ## Connection to server s0
	cd ansible_config \
	&& ssh -F ssh.cfg s0.infra

connect_s1: ## Connection to server s1
	cd ansible_config \
	&& ssh -F ssh.cfg s1.infra

connect_s2: ## Connection to server s2
	cd ansible_config \
	&& ssh -F ssh.cfg s2.infra

connect_s3: ## Connection to server s3
	cd ansible_config \
	&& ssh -F ssh.cfg s3.infra

connect_s4: ## Connection to server s4
	cd ansible_config \
	&& ssh -F ssh.cfg s4.infra

delete_agent: ## delete ip in know_hosts
	ssh-keygen -R 192.168.50.20 \
	&& ssh-keygen -R 192.168.50.21 \
	&& ssh-keygen -R 192.168.50.22 \
	&& ssh-keygen -R 192.168.50.23

## —— Fix with ansible demo 1 ———————————————————————————————————————————————————————————
fix_demo_un: instance_start delete_agent
	cd ansible_config \
	&& ansible-playbook playbook.yml --tags web

## —— Fix with ansible demo 2 ———————————————————————————————————————————————————————————
fix_demo_deux: instance_start delete_agent
	cd ansible_config \
	&& ansible-playbook playbook.yml 

## —— Others 🛠️️ ———————————————————————————————————————————————————————————————————
help: ## Liste des commandes
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
