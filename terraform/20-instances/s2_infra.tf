resource "aws_instance" "s2_php_apache" {
  ami           = "ami-00662eead74f66895"
  instance_type = "t2.micro"
  key_name      = aws_key_pair.ssh_terraform.key_name
  # private_dns   = "s2.infra"

  network_interface {
    network_interface_id = aws_network_interface.s2_network_interface.id
    device_index         = 0
  }

  tags = {
    Name = "s2-php-apache-provisionning-pmn"
  }
}

resource "aws_network_interface" "s2_network_interface" {
  subnet_id       = data.aws_subnet.data_subnet_private.id
  private_ips     = ["192.168.50.21"]
  security_groups = [aws_security_group.sg_s1_to_s4.id]

  tags = {
    Name = "s2-network-interface-provisionning-pmn"
  }
}