data "aws_instance" "data_s0" {
  filter {
    name   = "image-id"
    values = ["ami-00662eead74f66895"]
  }

  filter {
    name   = "tag:Name"
    values = ["s0-infra-provisionning-pmn"]
  }
}