resource "aws_subnet" "subnet_public" {
  vpc_id                  = aws_vpc.main_vpc.id
  cidr_block              = "192.168.50.0/28"
  availability_zone       = "eu-west-3a"
  map_public_ip_on_launch = false

  tags = {
    Name = "subnet-public-provisionning-pmn"
  }
}

resource "aws_subnet" "subnet_private" {
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = "192.168.50.16/28"
  availability_zone = "eu-west-3a"

  tags = {
    Name = "subnet-private-provisionning-pmn"
  }
}