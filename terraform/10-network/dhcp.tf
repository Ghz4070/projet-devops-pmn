resource "aws_vpc_dhcp_options" "main_vpc_dhcp" {
  domain_name          = "infra"
  domain_name_servers  = ["192.168.50.5"]

  tags = {
    Name = "dhcp-provisionning-pmn"
  }
}

resource "aws_vpc_dhcp_options_association" "dns_resolver" {
  vpc_id          = aws_vpc.main_vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.main_vpc_dhcp.id
}